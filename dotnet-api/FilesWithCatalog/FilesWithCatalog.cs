﻿// Copyright (c) 2011 DeltaXML Ltd. All rights reserved.
// $Id$

using System;
using System.Collections.Generic;
using System.Text;
using DeltaXML.CoreS9Api;
using System.IO;
using net.sf.saxon.s9api;

namespace DeltaXML.Demos
{
    /// <summary>
    /// Demontrates the use of the built-in catalog resolver.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The DTD referenced in the DOCTYPE of the samples files (a.xml and b.xml) does not exist,
    /// therefore the catalog is required to provide the DTDs. Furthermore, the DTD provides a
    /// default attribute (/root/@i='42') that does not appear in the input data, but does appear
    /// in the full-context delta result.
    /// </para>
    /// <para>
    /// In order for this example to work, the catalog resolver needs to be configured so that
    /// it can locate its catalog file(s). This is  done using a DeltaXML configuration
    /// properties file to set the resolver value (see deltaxmlConfig.xml in this directory).
    /// </para>
    /// </remarks>
    /// <seealso><a href="http://www.xml.com/lpt/a/1378">http://www.xml.com/lpt/a/1378</a></seealso>
    /// <seealso><a href="http://xml.apache.org/commons/components/resolver/resolver-article.html">http://xml.apache.org/commons/components/resolver/resolver-article.html</a></seealso>
    /// <seealso><a href="http://xerces.apache.org/xerces2-j/faq-xcatalogs.html">http://xerces.apache.org/xerces2-j/faq-xcatalogs.html</a></seealso>
    class FilesWithCatalog
    {
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory("../..");
            PipelinedComparatorS9 pc = new PipelinedComparatorS9();
            pc.setComparatorFeature("http://deltaxml.com/api/feature/isFullDelta", true);
            pc.setOutputProperty(Serializer.Property.INDENT, "yes");
            pc.compare(new FileInfo("a.xml"), new FileInfo("b.xml"), new FileInfo("out.xml"));
        }
    }
}
