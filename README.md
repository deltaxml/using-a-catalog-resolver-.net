# Using a Catalog Resolver

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/using-a-catalog-resolver`.*

---

A code sample to show how to instruct XML Compare to make use of catalog files.

This document describes how to run the sample. For concept details see: [Using a Catalog Resolver](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/using-a-catalog-resolver)

## Running the Sample
A Visual Studio solution and project are provided in the *dotnet-api* directory. You can run this in the usual way from Visual Studio.

# **Note - .NET support has been deprecated as of version 10.0.0 **